## USER TERMS AND CONDITIONS
**for** [**www.fooshapp.com**](https://www.google.com/url?q=http://www.fooshapp.com&sa=D&source=editors&ust=1614101099581000&usg=AOvVaw3rORcjiWlU1ijlPjBtUcnS)
**Foosh App Mobile Application**

This document is published to state the rules and regulations and Terms and Conditions for access and/or usage of [www.fooshapp.com](https://www.google.com/url?q=http://www.fooshapp.com&sa=D&source=editors&ust=1614101099582000&usg=AOvVaw2G3NcGqEE3us3fAJoq-SOi) (defined as the “Website”) and the Foosh Mobile Application (defined as “Foosh App”).

The Foosh App is a loyalty and rewards program that seeks to introduce Users of the Foosh App to the Products and Services of selected Merchants and to reward Users for their ongoing purchases from these Merchants.

All users (defined as “User”) of the Website and/or Foosh App (defined jointly or severally as the “Platforms” or “Platform”)) should carefully read the entire contents of this Terms and Conditions (defined as “TAC”) as it contains important information, which relates to the User’s legal rights and obligations. By accessing and/or using the Platforms, the User expressly agrees to be legally bound by this TAC. This TAC constitutes a legally binding agreement between the User and Foosh Limited (defined as “FOOSH”), the owner of the Platforms, in relation to the Platforms.

BY CONTINUING TO USE THE PLATFORMS USERS EXPRESSLY AGREE TO FOOSH’S TERMS AND CONDITIONS STATED HEREIN WITHOUT ANY RESERVATION. IF YOU DO NOT ACCEPT ANY OF THE TERMS AND CONDITIONS OF USE EXPRESSED HEREIN YOU MUST IMMEDIATELY CEASE TO USE AND EXIT THE PLATFORMS.

FOOSH RESERVES AT ALL TIMES THE RIGHT TO MAKE CHANGES TO THIS TAC AT ITS SOLE AND ABSOLUTE DISCRETION WITHOUT FURTHER REFERENCE TO YOU. SHOULD YOU BE UNHAPPY WITH ANY FUTURE CHANGES FOR ANY REASON WHATSOEVER, YOU MUST IMMEDIATELY CEASE TO USE THE PLATFORMS. IF USERS CONTINUE TO USE THE PLATFORMS THEY SHALL BE DEEMED TO EXPRESSLY AGREE TO EACH INSTANCE, WE MAKE SUCH CHANGES.

In this TAC, unless there is something in the subject matter or context inconsistent therewith, the following terms and expressions will have the following meanings:


|“Business Day”|Any day on which banks are legally open for business in England.|
| - | - |
|“CCPA”|California Consumer Privacy Act 2020|
|"Content"|Any and all images, text, information, data, audio, video, graphics, computer code, software, algorithms, text and any other material provided on or through the Platforms.|
|“Cookies”|Cookies are tiny files which contain slivers of data and information, which may or may not include, an anonymous unique identifier in relation to whom the cookie is directed. It is a tiny file that a website transfers and stores on your device with the purpose of tracking your movements within the Platforms and your interaction with the various components and features of the Platforms. These files facilitate to improve and enhance the user’s experience and record their preferences. Our cookies do not seek to control your devices nor are they damaging, and nor do they extract your personal information from your device. Our cookies will reside on your device(s) until they expire or are deleted by You.|
|“Flash Offers ” |Flash messages posted by the Merchant on the Platforms on short notice offering special deals for Users to purchase the Products and Services.|
|<p>“FOOSH” "or “Us” or</p><p>“We” </p>|Quantum Boost Ltd, a company incorporated in England and Wales, being the owner of the Website and the Foosh App, jointly or severally referred to as the Platforms or Platform as the context requires.|
|“Foosh App”|The Foosh Mobile Application for use on smart phones and other mobile devices.|
|“GDPR”|Regulation (EU) 2016/679 General Data Protection Regulation|
|“Intellectual Property”|Intellectual Property of FOOSH as produced on the Platforms shall incorporate the entire content of the Platforms without exception and shall include, but is not limited to, all text, documents, information, data, articles, images, photographs, graphics, software, algorithms applications, video recordings, audio recordings, sounds, designs, features, trade name, service mark, trade dress, logo, custom graphics or icons and other materials that are or may become available on the Platforms after the effective date of this TAC and it is hereby declared that the entirety of the Intellectual Property is the sole and exclusive property of FOOSH . By using the Platforms, the User does not in any way acquire any interest, express or implied, in the Intellectual Property.|
|“Invitations”|The invitations posted by Merchants on the Platforms in the form of Loyalty Cards and Flash Offers inviting the User to make purchases of goods and services from them.|
|“License”|The License granted by FOOSH to the User pursuant to Clause 7 of the TAC.|
|“Loyalty Cards”|Information posted by Merchants on the Platforms relating to their products and services inviting the User to purchase their goods and services on the indicated terms. Such Loyalty cards may offer the User discounts on the purchase price of goods and services based on multiple purchases or other criteria as the Merchants may indicate.|
|“Merchant”|Third party business associates of FOOSH who post information on their products and services on the Platforms with a view to making sales of the products and services to the User and offering the User loyalty discounts.|
|“Platforms” or “Platform”|Jointly or severally as the context requires, [www.fooshapp.com](https://www.google.com/url?q=http://www.fooshapp.com&sa=D&source=editors&ust=1614101099590000&usg=AOvVaw2toZbMjthyQSg_jWOVRH50) and the Foosh App and all related software, applications and related services and all Intellectual Property therein.|
|“Products and Services”|The Products and Services made available for sale by Merchants and in respect of which the Merchant posts Loyalty Cards and Flash Offers on the Platform.|
|“Purchased Item(s)”|The Products and Services and/or other items purchased by the User directly from a Merchant through the redemption of the Loyalty Cards and/or Flash Offers posted on the Platforms.|
|“Purchaser”|Any Merchant who makes a payment to FOOSH in order to publish one or more of his/her/its Loyalty Cards and/or Flash Offers on the Platforms .|
|“Registered User”|The person or legal entity who creates a user account on the Platforms as a Registered User by following the prescribed registration process in order to be able to use the Platforms to earn, receive and redeem Loyalty Cards and/or Flash Offers posted on the Platforms by Merchant in respect of Products and Services offered for sale by Merchants.|
|“TAC”|The Terms and Conditions of this Platforms as published and are inclusive of all express and implied terms stated therein and include the Privacy Policy and Cookie Policy which form an integral part of the TAC except as otherwise provided.|
|“User” or “Users” or “You”|Any and all persons and/or entities that use any part of the Platforms for any purpose no matter the duration or circumstances of such use and shall for all purposes include Registered Users.|
|“Website”|[www.fooshapp.com](https://www.google.com/url?q=http://www.fooshapp.com&sa=D&source=editors&ust=1614101099594000&usg=AOvVaw01AkEtWeQXQASsLukklV0t), which is owned, developed and operated by FOOSH.|

### 1. Language

The official language of the Platforms is English. All registrations procedures and all communications must be in English.

### 2. Acceptance Of The TAC By Users

2.1 All visitors to the Platforms must read this TAC before using any part of the Platforms. If you have any issue with any provision set out herein and/or you do not wish to be bound by any of the terms and conditions, you must stop using the Platforms immediately and leave. 

2.2 By continuing to use any part of the Platforms for any duration and for any purpose, the User expressly and irrevocably without any reservation whatsoever accepts all the provisions, terms and conditions for the Platforms set out in this TAC. 

2.3 By clicking on a link You may be redirected to a Merchant’s website or to a third party website. You expressly and irrevocably consent to being redirected to the Merchant’s or the third party website. On landing on the Merchant’s or the third party website, You must read the Privacy Policy and the Terms and Conditions of that website. By continuing to use the Merchant’s or the third party website You are deemed to accept the Privacy Policy and the Terms and Conditions of that website. You may also be required to register an account on the Merchant’s or the third party website. 



### 3. Intellectual Property And Copyright

3.1 The entire content of the Platforms is the Intellectual Property and copyright of FOOSH who reserves all rights to the Intellectual Property and copyright represented by the entire content of the Platforms. No party is authorised or entitled to exploit commercially the content of the Platforms , in whole or any part thereof, under any circumstances and for any reason. Any copying, reproduction, redistribution, sharing and/or transmission of the content of the Platforms or any part thereof without the prior written consent of FOOSH is strictly prohibited and is an infringement of the relevant laws.

3.2 Any trademarks displayed on the Platforms are the trademarks of FOOSH and no person or entity shall have the right to use or deal in the trademarks in any way whatsoever.

3.3 Any non-proprietary postings made by Users on any part of the Platforms shall become the exclusive property of FOOSH.

3.4 Notwithstanding the provisions of Clause 3.3, the User making the non-proprietary posting shall be wholly responsible for the entire content of the posting for all purposes, including but not limited to breach of copyright, theft of Intellectual Property, libel, slander, racial discrimination, sexual discrimination and any other instance which may give rise to any liability of whatever nature on the part of FOOSH.

3.5 FOOSH shall at all times retain the right, at its sole and absolute discretion, to remove any posting made by a User on the Platforms , in whole or in part, and FOOSH shall be under no obligation to give a reason for such removal or to refer to the User.

3.6 No User is authorised to download, use, manipulate, trade or deal in any and all images posted and displayed on the Platforms. Users may share images via social media exclusively in the manner provided for sharing on the Website.

3.7 If you are the creator/owner of any material displayed on the Platforms and you have not granted permission to anyone to use such material on the Platforms , please advise FOOSH in writing of the possible breach of your intellectual property rights and FOOSH undertakes to investigate the matter and where deemed appropriate, after such investigation, remove such material from the Platforms . All such notifications should be sent to FOOSH at <admin@fooshapp.com>.



### 4. Intellectual Property And Copyright Of Merchants

Notwithstanding the provisions of Clause 3 above, the following provisions shall apply in respect of any images and other materials posted on the Platforms by Merchants:

4.1 Each Merchant who allows posting of their intellectual property on the Platforms shall be solely responsible in every respect for such posting and displaying of such images and FOOSH makes no representations, undertaking or warranties in respect of such postings and takes no responsibility for such postings and the content thereof.

4.2 All intellectual property and copyright of Merchants posted on the Platforms shall at all times remain vested in the Merchants. At no time shall any intellectual property or copyright of whatever nature pass to FOOSH, any User of the Platforms or any other third party who has access to the content of the Platforms, whether such access is with consent or without consent of FOOSH.

4.3 The Merchant in respect of any of his/her/its intellectual property displayed on the Platforms or images thereof grants to FOOSH a royalty free and fee free worldwide license to use and display images, videos and other displays and information on the Platforms and such license shall continue in full force and effect until such time that the Merchant cancels the license in writing at which time FOOSH agrees to delete all images, videos and other displays and information from the Platforms. 

### 5. Registration

5.1 All Users seeking to have full access to the Platforms and Products and Services by redemption of the Loyalty Cards and/or Flash Offers must complete the registration process and be accepted by FOOSH as a Registered User of the Platforms and the acceptance of any registration shall be at the sole and exclusive discretion of FOOSH who shall be under no obligation to provide any explanation should any registration be declined and where such an explanation is provided it shall be provided as a courtesy only.

5.2 The User may use their personal or business email to complete the registration process or you may use either your Apple, Facebook or Google account to complete the registration process. In creating the account, the Registered User represents, undertakes and warrants to FOOSH that they are not barred from accessing the Platforms or receiving the content and services in his/her/its jurisdiction and that the information provided for the creation of the account is true and correct and the Registered User agrees that he/she/it shall keep such information updated as changes occur from time to time in a timely manner.

5.3 If the Registered User uses their Apple, Facebook or Google account to complete the registration process, the Registered User expressly and irrevocably authorizes FOOSH to access certain information in the Registered User’s Apple, Facebook and Google accounts for the purpose of verifying the Registered User’s identity.

5.4In creating his/her/its account, the Registered User shall not use a username, display name or profile picture that may reasonably be expected to cause offense to others and they shall not use any username, which have or may have profane, racial, sexual or other socially unacceptable references.

5.5 As part of the registration process, all registrants are required to answer a series of mandatory questions and a series of optional questions. The answers to these questions will assist FOOSH to determine whether the registrant is a suitable candidate to become a Registered User and determine the most suitable services for the registrant.

5.6 FOOSH at all times reserves in its sole and absolute discretion the right to terminate or suspend the Registered User’s account if any of the information provided to FOOSH by the Registered User is found to be misleading, incorrect or false. FOOSH may take such action it sees fit at any time without giving notice to the Registered User and/or without any reference to the Registered User.

5.7 The Registered User shall at all times be responsible for any activity that occurs in or through his/her/its account without exception and shall be responsible for its security. The Registered User shall not at any time and for any reason share his/her/its account with any other person or legal entity and shall keep the Username and the password private. In the event the password is lost or stolen or has been disclosed to a third party, whether consciously or inadvertently, the Registered User undertakes to inform FOOSH of the breach of security of his/her/its account immediately. Notwithstanding the acceptance of such responsibility by the Registered User for the contents of his/her/its account, the Registered User shall not create any content which:

a) Engages in any activity that interferes with or disrupts access to or otherwise causes harm to the Platforms (or the servers and networks);

b) Impersonates any person or entity, or falsely states or otherwise misrepresents the Registered User’s affiliation with a person or entity;

c) Is racist, bigoted, sexually discriminating, sexual in nature, harassing, obscene, profane, offensive or which may incite hatred of any kind or is illegal or which may promote illegal acts to be committed or which the Registered User knows or believes are untrue, inaccurate false, malicious, libellous or slanderous or are in any way linked or aligned to or with any terrorist group or groups or in any way incite or promote acts of terrorism and/or violence however defined or contain instructions of any kind which may result in the commissioning of acts of violence and terrorism;

d) Creates unlawfully threatening or unlawfully harassing content of any kind including but not limited to indecently representing a person of any gender and/or age;

e) Uploads or distributes files containing viruses, corrupted files, or other software or programs that may damage the operation of the Platforms, servers or networks;

f) Probes, scans or tests the vulnerability of the Platforms or any connected networks, or breach security or authentication measures. You may not reverse look-up, trace or seek to trace any information on any other user, of or visitor to the Platforms, or any other customer or vendor to its source, or exploit the Loyalty Cards, Flash Offers and the Products and Services or information made available or offered in any way, including but not limited to personal identification information other than Your own; 

g) Collects or stores data about other users in connection with the prohibited conduct and activities in this Clause;

h) May create liability for FOOSH or cause it to lose (in whole or in part) the services of its internet service provider, vendors or other parties necessary for the operation and functioning of the Platforms in all aspects; 

i) Could lead to the exploitation of any person under the age of 18 for any purposes, including, but not limited to, those of a sexual, political, racial and/or economic nature;

j)Relates to a ponzi scheme, pyramid or other investment scheme, chain letter, unsolicited mailing and other similar activities, including those related to money laundering in any form;

k)Infringes or violates the rights of any third party, directly or indirectly, including but not limited to, rights to copyright or intellectual property and rights to privacy; and

l) Solicits passwords and other security access information

m) Uses the Platforms or any material or Content for any purpose that is prohibited by this TAC or unlawful in any jurisdiction or to solicit the performance of any illegal activity or other activity and which is not already covered in the other provisions of this Clause.

5.8 Under no circumstances will FOOSH be liable to the User for any direct, indirect or consequential loss or loss of profits, goodwill or damage resulting from the disclosure of your username and/or password. The User further agrees to reimburse FOOSH for any improper, unauthorised or illegal use of your account by you or by any person obtaining access to the Platforms, the Loyalty Cards, the Products and Services or otherwise by using your designated username and password, whether or not you authorized such access.

5.9 FOOSH reserves the right to undertake such actions, consultations it deems appropriate or necessary to check and verify the identity of the Registered User and/or the information provided and may include checking into any criminal background or search of various registers and the Registered User hereby expressly consents to such checks and verifications.

### 6. Who May Use The Platforms

6.1 Use of the Platforms is strictly restricted to persons over the age of 18 and who can enter into a legal agreement in their jurisdiction. The User expressly warrants and undertakes that he/she/it has the capacity and authority to enter into this TAC Agreement with FOOSH.

6.2 Unregistered users are granted limited access to the Platforms to allow them an opportunity to form a view as to whether they would like to register on the Platforms to gain access to all the features and benefits.

6.2 No person or entity shall be entitled to have more than one account on the Platforms. Any breach may result in any or all accounts being terminated at the sole and absolute discretion of FOOSH.

6.3Registered Users shall only have access to the Platforms for so long as they observe all the terms and conditions set out in this TAC and its related documents which shall include but are not limited to, the Privacy Policy, Cookies Policy and Refunds Policy (if any).

### 7. License To Use

7.1 Subject to the terms and conditions in this TAC, FOOSH grants Users over the age of 18 a license to use the Platforms purely for personal and non-commercial purposes and such license is non-exclusive and limited in nature and is personal to the User and shall not be transferable in any way. Except for this limited license, the User shall not have any other entitlement to use the Platforms and shall be entitled to use and/or exploit the Platforms for any commercial purpose(s).

7.2 The User shall not use the Platforms for any commercial use, sexual or racial harassment, bullying, immoral use, illegal use (in any jurisdiction), conspiracy to commit crimes and/or acts of terrorism, gambling or any other use which FOOSH shall in its absolute discretion deem to be nefarious, obnoxious or objectionable or which could render FOOSH in danger of criminal and or civil liability in any jurisdiction and/or which could in any way damage the reputation of FOOSH or its commercial and economic viability. 

7.3 FOOSH makes no grant of any implied license or other implied rights of any kind whatsoever for the use of the Platforms, whether temporary or permanent.

7.4 FOOSH offers no assurance of guarantee of any kind that the User will be able to access the Platforms on a continuous on demand basis nor does FOOSH in any make any representation as to the suitability of the Platforms for the User. The Platforms will not be accessible by the User during periods of maintenance, routine or otherwise, and during periods of disruption which are beyond FOOSH’s control. The User makes use of the Platforms entirely at their own risk and FOOSH shall not in any way be responsible for any loss suffered by the User due to the inaccessibility and/or unsuitability of the Platforms.

7.5 Notwithstanding the grant of the License pursuant to this Clause, FOOSH at all times retains the right to make any changes to the Platforms that it sees fit at its sole and absolute discretion and FOOSH shall be under no obligation to give the User prior notice of such changes. If required, FOOSH shall at all times be at liberty to change the business model of the Platforms, in whole or in part, without any reference to the User.

### 8. Refusal Of Use

Notwithstanding the Licence granted to the User pursuant to Clause 7, the use of the Platforms shall at all times be at the sole and absolute discretion of FOOSH who shall at all times be at liberty to refuse use of the Platforms to any person or entity without explanation. Such right of refusal may be exercised before or after the Registered User makes purchases of Products and Services from Merchants. 

If refusal of service is after placement of an order only in respect of any goods and services FOOSH itself offers on the Platforms such as FOOSH merchandise, FOOSH shall give a refund provided always that the order has not already been executed and/or performed, in part or in whole. The giving of such refund shall be an act good customer service and shall not be interpreted in any way whatsoever as an admission of liability, fault, responsibility and/or guilt for any purpose whatsoever. Where the Service has already been performed, in whole or in part, FOOSH shall be entitled to retain any payment in full or on a pro-rated basis.

### 9. Redemption Of Loyalty Cards And Flash Offers

9.1 The functions offered by FOOSH on the Platforms allow Merchants to post details of the Products and Services they have available for sale. The User can purchase the Products and Services directly from the Merchants on the indicated terms. Part of the Merchant’s terms of sale relate to Loyalty Cards and/or Flash Offers. The Loyalty Cards allow the User to secure discounts from Merchants as a result of multiple/frequent purchases of Products and Services. These discounts can be applied by the User in respect of future purchases of the Products and Services and/or they can be shared with friends and family. Flash Offers may be posted by Merchants on the Platforms making available to the User special offers or discounts to be applied when purchasing the Products and Services.Users do not make any purchases from FOOSH except for any Platform related merchandise that it may offer from time to time. The Loyalty Cards posted by Merchant shall be valid and redeemable only in the Jurisdiction of the Merchant posting it unless the terms of the Loyalty Cards indicate otherwise. If an expiry date is indicated, the Loyalty Cards must be redeemed by the indicated date failing which it will expire for all purposes. It is the responsibility of the User to check the terms of each Loyalty Card and the terms and conditions for redemption of the discounts.

9.2 FOOSH expressly states that it does not own, directly or indirectly, any of the Products and Services to which the Loyalty Cards and/or Flash Offers relate to . FOOSH only acts as a third party facilitator bringing together buyers and sellers through its Platforms. Neither does FOOSH act as an agent of any Merchant who posts Loyalty Cards and Flash Offers on the Platforms. FOOSH makes no representations or warranties in respect of any Loyalty Cards and Flash Offers posted by a Merchant or in respect of any representations and claims made by the Merchants or in respect of any Products and Services offered for sale by Merchants. The User shall at all times be responsible for making their own assessment in regard to the suitability of redeeming any Loyalty Cards, taking up any Falsh Offers posted on the Platforms.

9.3 The Loyalty Cards and the attached discounts relating to the Products and Services of the Merchants issuing the Loyalty Cards are personal to you and may not be transferred to any other person and/or entity except as specifically allowed by the Foosh App. The Loyalty Cards Flash Offers may only be used for the redemption of the attached discounts/offers in respect of the Products and Services of the Merchant that issued them They cannot be redeemed or applied with any other non-issuing Merchants Loyalty Cards. The User may not use the Loyalty Cards and/or Flash Offers for any commercial purposes or for any purpose not expressly authorised in this TAC.

9.4 The display of Products and Services on the Platforms by Merchants does not constitute an offer for sale by either FOOSH or the Merchants. The display of such items by a Merchant is a non-binding invitation to the User to submit an offer to the Merchant to purchase an item directly from the Merchant by visiting their place of business or website or other means as indicated by the Merchant. Once submitted, the offer may be accepted or rejected at the sole and absolute discretion of the Merchant. For all purposes, the supplier of any Product and Services to the User shall be the Merchant.

9.5 All prices displayed on the Platforms are posted by Merchants and, unless indicated otherwise, are in respect of in store purchases and are stated inclusive of all taxes payable. Any out of store purchases are subject to additional charges such as packing and delivery costs. Any amounts due in respect of delivery charges, taxes of any kind will be added by the Merchant. All delivery matters shall be the sole responsibility of the Merchant and FOOSH accepts no responsibility.

9.6 As the User will be using the Foosh App to redeem the Loyalty Cards and Flash Offer to purchase the Products and Services from the Merchant, FOOSH will provide some of your personal details to the Merchants which will required by the Merchant to complete the transaction. Your relevant personal details will be transferred to the Merchant through the Foosh App and the User expressly agrees to the transfer of such details. The Merchant may require additional information from the User to complete the transaction.

9.7 FOOSH does not in any way warrant or guarantee that the Merchant will honour any of the Loyalty Cards and/or Flash Offers the Merchant posts on the Platforms. As the Loyalty Cards and Flash Discounts are not paid for items, Merchants shall be entitled to withdraw them without notice or they may make changes to the terms and conditions of redemption. FOOSH also does not warrant or guarantee that the Merchant will have ready stock of the Products and Services at the User’s desired time of purchase. The Merchant shall be entitled to offer the User an alternative Product or Service of equivalent Value failing which the User shall waitfor restocking of the Product or Service or forgo purchase thereof without any penalty or liability to FOOSH or the Merchant.

9.8 Where the Products and Services offered by a Merchant are of a nature that bookings/reservations are required, the User must strictly abide by the cancellation policy of the Merchant. If the User fails to follow the cancellation policy for any reason, the User agrees that the Merchant shall be at liberty to charge a reasonable late cancellation free and the Merchant shall also beat liberty to decline any future bookings/reservations by the User.

9.9 FOOSH may assign, transfer, delegate or otherwise dispose of its rights and obligation pursuant to the TAC, in whole or in part, without having to give the Registered User any notice. The Registered User may not under any circumstances assign this TAC or assign, transfer, delegate any rights and/or obligations hereunder, in whole or in part.

9.10 In the event that any of the provisions of this Clause 9 become legally invalid or unenforceable for any reason, the remainder of the provisions shall remain unaffected and shall continue to be binding on the Registered User and FOOSH.

9.11 FOOSH reserves the right at all times to change the provisions of this Clause 9 without giving any prior notice to Users and the onus shall be on Users to check the provisions of Clause 9 and this TAC as a whole prior to entering any transactions with the Merchant.

9.12 As FOOSH does not itself make any sales of the Products and Services to the User, it does not operate any refund policy in respect thereof. The purchases by the User of the Products and Services are made directly from the Merchants and payment is made directly to them, the User must resort to the Merchant for any refunds in respect of the Products and Services. For any FOOSH merchandise sold by FOOSH on the Platforms, the User shall be entitled to submit a request for a refund to FOOSH in writing within fourteen (14) days of payment. The User shall not be entitled to any refund if the item(s) ordered have already been personalized to the requirements of the User before the refund request is received by FOOSH. On receipt of a valid written refund request, the refund will be processed within fourteen (14) days and the funds will only be refunded to the source from which they originated. A refund will not be processed in any other manner. After the lapse of the fourteen (14) Day Period, the User’s right to a refund shall terminate.


9.13 The User shall at all times be responsible for keeping track of his/her/its loyalty bonuses, discounts and Flash Offers .

9.14 The place of performance as regards payment and delivery shall be England unless expressly indicated otherwise by FOOSH notwithstanding that the actual place for payment and delivery occurred at some other place.

9.15 The User agrees that any review and/or comment made by him/her/it on social media or any other channel or medium shall be made on a truthful, fair and objective basis and shall not contain any slanderous, libellous, defamatory disparaging, obscene, sexist, racial or rude statements which may, intentionally or unintentionally, damage the reputation and business of FOOSH, Foosh App and the Merchants.

### 10. Sharing Discounts

10.1 The Foosh App features a function that allows the User to share his/her/its accumulated discounts and Flash Offers with friends and family. In order to share the discounts, the person with whom the items is/are to be shared with must also download and install the Foosh App on their device. The nominated friend or family for the sharing must accept the terms and conditions of this TAC and agree to some of their personal details being shared with the Merchants. After installation, the User may use the Share Discount function to share their accumulated discounts.

10.2 The discount that is shared can only be used by the sharing party with the Merchant who issued the original Loyalty Card giving rise to the discount and only at the specific outlet in respect of which it was issued.

10.3 For the Share Discount function to work, the party shared with must give their consent to share their personal information with the Merchant. If the sharing party does not consent, the sharing of the discount cannot proceed.

### 11. Obligations Of The User And Warranties

11.1 With the exception of social media sharing purposes using the features provided on the Platforms, Users shall not distribute, modify, recreate, reverse engineer, create derivative works from, transfer, or sell any information obtained from the Platforms, for commercial or non-commercial purposes, and whether in modified form or not..

11.2 The User agree not to access (or attempt to access) the Platforms or Loyalty Cards or Flash Offers by any means other than through the interface that is provided by the Platforms. The use of deep-link, robot, spider, scraping or other automatic device, program, algorithm or methodology, or any similar or equivalent manual process, to access, acquire, copy or monitor any portion of the Platforms or content, or in any way reproduce or circumvent the navigational structure or presentation of the Platforms, materials or any content, to obtain or attempt to obtain any materials, documents or information through any means not specifically made available through the Platforms is expressly prohibited.

11.3 Users shall not engage in advertising to, or solicitation of, other Users of the Platforms to buy or sell any products or services images of which have not been posted and displayed on the Platforms by FOOSH or the Merchants.

11.4 It shall be a violation of this TAC to use any information obtained from the Platforms in order to harass, abuse, or harm another person. FOOSH shall (and the User hereby expressly authorise us to) disclose any information to law enforcement or other government officials necessary or appropriate in connection with the investigation and/or resolution of possible crimes, especially those that may involve personal injury or which involve persons under the age of 18. This may include, without limitation, disclosure of the information in connection with an investigation of alleged illegal activity or solicitation of illegal activity or in response to a lawful court order or subpoena.

11.5 FOOSH has no obligation to monitor the materials posted on the Platforms . FOOSH shall have the right to remove or edit any content that in our sole discretion violates, or is alleged to violate, any applicable law or either the spirit or letter of this TAC. THE USER REMAINS SOLELY RESPONSIBLE FOR THE CONTENT OF THE MATERIALS POSTED ON THE Platforms. In no event shall FOOSH assume or have any responsibility or liability for any content posted or for any claims, damages or losses resulting from use of content and/or appearance of content on the Platforms. You hereby represent and warrant that you have all necessary rights in and to all content which you provide, and all information it contains, and that such content shall not infringe any proprietary or other rights of third parties or contain any libellous, tortuous, or otherwise unlawful information. 

11.6 The User hereby warrants to FOOSH:

a) It will not be required to pay any royalties in respect of any of the User’s content and will not need to seek any licenses, approvals or consents from any party in respect of the contents;

b) That the contents do not contain any of the information prohibited this TAC ;

c) The content posted by the User will not infringe any third party rights, their intellectual property or their right to privacy; and

d) The content posted by the User conforms and complies with all the laws of the User’s jurisdiction.

### 12. Disclaimer Of Warranties And Liabilities

12.1 No provision in this Clause shall exclude the liability of FOOSH to you in respect of:

a) Part I of the Consumer Protection Act 1987;

b) Section 12 of the Sale of Goods Act 1979;

c) Section 2 of the Supply of Goods and Services Act 1982;

d) In cases of personal injury or death caused by our negligence;

e) Fraud, breach of trust or fraudulent misrepresentation. 

12.2 TO THE MAXIMUM EXTENT ALLOWED BY APPLICABLE LAWS, USERS EXPRESSLY AGREE THAT THE PRINCIPLE OF ‘CAVEAT EMPTOR’ SHALL APPLY IN EVERY RESPECT AND TO EVERY PART OF THE PLATFORMS AND THE ENTIRETY OF THEIR CONTENT FROM TIME TO TIME AND THIS TAC.

THE SERVICES, CONTENT AND OTHER MATERIALS ARE PROVIDED BY THE PLATFORMS ON AN “AS IS” BASIS WITHOUT WARRANTY OF ANY KIND, EXPRESS, IMPLIED, STATUTORY OR OTHERWISE, INCLUDING ANY IMPLIED WARRANTIES AS TO FITNESS FOR PURPOSE, AS TO MERCHANTABILITY OF AN ITEM OR SERVICE, AS TO TITLE AND THE USER USES THE SERVICES AND THE PLATFORMS ENTIRELY AT HIS/HER/ITS OWN RISK AND FOOSH EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES AND REPRESENTATION OF ANY KIND, EXPRESS OR IMPLIED, THAT MAY ARISE IN ANY JURISDICTION THROUGH THE OPERATION OF LAW AND ANY WARRANTIES AND/OR REPRESENTATION IN RESPECT OF BUT NOT LIMITED TO THE FOLLOWING ARE EXPRESSLY DENIED BY FOOSH:

a) PURPOSE(S) IN VISITING THE PLATFORMS WILL BE MET, IN FULL OR IN PART;

b) SERVICES PROVIDED WILL IN ANY WAY BE UNINTERRUPTED, SECURE OR ERROR-FREE OR THAT THEY WILL BE PROVIDED IN ACCORDANCE WITH ANY SCHEDULE;

c) INFORMATION, RESULTS OR MATERIAL SECURED BY THE USER ON OR THROUGH THE PLATFORMS WILL IN ANY WAY BE RELIABLE, ERROR FREE OR EFFECTIVE;

d) THAT FOOSH WILL PROVIDE MAINTENANCE AND/OR UPDATES IN RESPECT OF ANY ERRORS OR DEFECTS IN THE PLATFORMS;

e) THE SERVICE AND/OR THE PLATFORMS IS IN ANY WAY LEGAL FOR USE IN THE JURISDICTION OF ANY USER;

f) THE PLATFORMS WILL FUNCTION ON A CONTINUOUS BASIS WITHOUT ANY INTERRUPTION OF ACCESS TO ANY PART;

g) ANY OF USER’S DATA WITHOUT LIMITATION SAVED ON FOOSH’S SERVER SHALL CONTINUE TO BE AVAILABLE TO THE USER ON A CONTINUOUS BASIS OR THAT IT WILL BE RETAINED FOR ANY GIVEN PERIOD OF TIME;

h) ANY CONTENT OF LINKED THIRD PARTY WEBSITES AND/OR SOCIAL MEDIA REFERENCED, ADVERTISED OR PROMOTED ON THE PLATFORMS; 

i) PRODUCTS OR SERVICES ADVERTISED BY ANY THIRD PARTY ON THE PLATFORMS;

j) ANY INJURY A USER MAY SUFFER, WHETHER TO PERSON, PROPERTY OR OTHERWISE AS A RESULT OF VISITING OR USING THE PLATFORMS IN ANY WAY AND/OR RELYING ON AND/OR TAKING ACTION ON ANY ITEM, INCLUDING INFORMATION, IMAGE, VIDEO, SOFTWARE, FOUND ON THE PLATFORMS IRRESPECTIVE OF WHO POSTED THE ITEM, INCLUDING ANY UNAUTHORISED THIRD PARTY;

k) ANY LOSS OF ANY ITEM, INCLUDING BUT NOT LIMITED TO ANY PERSONAL AND/OR CONFIDENTIAL INFORMATION, IMAGES, VIDEOS OR SOFTWARE, AS A RESULT OF ANY UNAUTHORIZED ACCESS TO YOUR ACCOUNT AND/OR FOOSH’S SERVERS;

l)ANY VIRUSES, WORMS, BUGS, TROJAN HORSES, TRACKERS AND OTHERS OF THE LIKE THAT MAY BE TRANSMITTED AND/OR DOWNLOADED TO THE USERS DEVICE(S) FROM AND/OR THROUGH THE PLATFORMS AND/OR FOOSH’S SERVERS; AND

FOOSH SHALL HAVE NO LIABILITY TO ANY USER IN RESPECT OF THE FOREGOING AND IN RESPECT OF ANY ITEM NOT EXPRESSLY STATED HEREIN AND WHICH IS A PART OF THE PLATFORMS IN ANY WAY AND SUCH LIABILITY IS EXCLUDED BY FOOSH TO THE MAXIMUM EXTENT PERMITTED BY THE APPLICABLE LAWS AND SUCH EXCLUSION SHALL APPLY WHETHER ANY RIGHT OF A USER ARISES OUT OF INTELLECTUAL PROPERTY RIGHTS, PRIVACY, PUBLICITY, OR OTHER LAWS. THE PLATFORMS ALSO DISCLAIM ALL LIABILITY WITH RESPECT TO THE MISUSE, LOSS, MODIFICATION OR UNAVAILABILITY OF ANY CONTENT OF THE USER, WHETHER POSTED BY THE USER OR SOME THIRD PARTY.

BY CONTINUING TO USE THE PLATFORMS, THE USER EXPRESSLY INDICATES THAT HE/SHE/IT UNDERSTANDS AND AGREES THAT ANY AND ALL MATERIAL OR DATA DOWNLOADED OR OTHERWISE FOUND THROUGH THE PLATFORMS IS DONE SO ENTIRELY AT THE USER’S OWN DISCRETION AND RISK AND THE PLATFORMS AND FOOSH ACCEPT NO LIABILITY WHATSOEVER FOR ANY ERRORS OR OMISSIONS, WITH RESPECT TO ANY INFORMATION OF WHATEVER NATURE PROVIDED TO THE USER WHETHER ON BEHALF OF ITSELF OR THIRD PARTIES.

NO INFORMATION, IMAGES, GRAPHICS, VIDEOS (OF ANY TYPE) PROVIDED AND/OR POSTED ON THE PLATFORMS BY FOOSH AND/OR ANY OTHER PARTY SHALL IN ANY WAY BE INTERPRETED AS THE GIVING OF ADVICE AND/OR A CALL TO ACTION. ANY AND ALL POSTINGS ON THE PLATFORMS ARE FOR INFORMATION PURPOSES ONLY AND USERS MUST EXERCISE THEIR OWN DISCRETION AS TO HOW THEY WISH TO USE SUCH INFORMATION. ANY ACTION TAKEN BY USERS AND/OR REFRAINED FROM TAKING ARE ENTIRELY AT THE USER’S DECISION AND RISK AND ANY HARM, INJURY OR LOSS THE USER SUFFERS AS A RESULT IS ENTIRELY THE USER’S OWN RISK AND RESPONSIBILITY AND FOOSH SHALL NOT BE RESPONSIBLE OR LIABLE IN ANY WAY FOR SUCH HARM, INJURY OR LOSS.

NONE OF THE PROVISIONS OF THIS CLAUSE SHALL AFFECT THE RIGHTS OF THE USER’S STATUTORY RIGHTS IN THE UNITED KINGDOM.

### 13. Indemnification And Limitation Of Liability

THE USER HEREBY AGREES UNCONDITIONALLY AND IRREVOCABLY TO INDEMNIFY AND HOLD HARMLESS THE PLATFORMS AND FOOSH, ITS SUCCESSORS AND ASSIGNS, AND ITS DIRECTORS, SHAREHOLDERS, OFFICERS AND EMPLOYEES FROM AND AGAINST ANY AND ALL ACTIONS BROUGHT BY ANY THIRD PARTY AND ANY ARISING LOSSES, LIABILITIES, COSTS AND OTHER EXPENSES OF WHATEVER NATURE, INCLUDING BUT NOT LIMITED TO, ATTORNEY FEES, WHERE SUCH ACTION ARISES FROM:

a) THE ACTIVITIES OF THE USER ON THE PLATFORMS;

b) A VIOLATION BY THE USER OF ANY TERMS OF THE LICENSE GRANTED BY THE PLATFORMS AND FOOSH; 

c) CONTENT POSTED BY THE USER ON THE PLATFORMS;

d) ANY CONTENT OR MATERIAL TRANSMITTED BY THE USER THROUGH THE SERVICES AND/OR THE PLATFORMS IN VIOLATION OF ANY LAWS AND/OR REGULATION OR INFRINGES THE RIGHTS OF ANY THIRD PARTY OR THEIR RIGHT TO PRIVACY; AND/OR

e) A BREACH OF ANY OF THE TERMS AND CONDITIONS OF ANY THIRD-PARTY WEBSITE TO WHICH THE USER IS REDIRECTED TO THROUGH A LINK PROVIDED BY THE PLATFORMS AND FOOSH.

NOTWITHSTANDING ANYTHING TO THE CONTRARY IN THIS TAC, THE USER EXPRESSLY AGREES THAT THE LIMIT OF LIABILITY OF THE PLATFORMS AND FOOSH TO THE USER IN ANY DISPUTE SHALL BE THE VALUE OF ANY PURCHASE MADE BY THE USER THROUGH THE USE OF THE SERVICES AND IF NO PURCHASE IS MADE , LIABILITY SHALL BE LIMITED TO POUND STERLING ONE ( 1.00). 

THE USER EXPRESSLY ACKNOWLEDGES THAT THE PLATFORMS AND FOOSH SHALL NOT BE LIABLE TO HIM/HER/IT IN RESPECT FOR ANY OTHER LOSS OR HARM, ECONOMIC OR OTHERWISE, HOWEVER SUFFERED, INCLUDING BUT NOT LIMITED TO ANY INDIRECT, INCIDENTAL, CONSEQUENTIAL , SPECIAL OR PUNITIVE DAMAGES FOR ANY PERSONAL INJURY, PAIN AND SUFFERING, EMOTIONAL DISTRESS, LOSS OF REVENUE AND/OR PROFITS, LOSS OF SAVINGS, LOSS OF GOODWILL AND/OR REPUTATION, LOSS OF DATA AND/OR INFORMATION OF ANY TYPE, BREACH OF CONTRACT, NEGLIGENCE OF ANY NATURE AND FURTHER, THE USER EXPRESSLY AGREES THAT THE PLATFORMS AND FOOSH SHALL NOT BE LIABLE TO HIM/HER/IT FOR ANY EQUITABLE AND/OR LIQUIDATED DAMAGES OF WHATEVER NATURE FOR ANY LOSS HOWSOEVER ARISING. 

THE USER EXPRESSLY ACKNOWLEDGES THAT THE EXCLUSION OF LIABILITY BY THE PLATFORMS AND FOOSH FORMS A FOUNDATION OF THE BASIS ON WHICH THE PLATFORMS AND FOOSH GRANTS THE USER A LICENCE TO USE THE PLATFORMS ON THE TERMS AND CONDITIONS SET OUT IN THE TAC. IF THE USER IS IN ANY WAY DISSATISFIED WITH THE LIMITATION OF LIABILITY AS EXPRESSED IN THIS CLAUSE SPECIFICALLY AND THIS TAC GENERALLY, THE USER MUST IMMEDIATELY TERMINATE HIS/HER/ITS ACCOUNT AND CEASE USING THE SERVICES AND THE PLATFORMS FOR ANY PURPOSE.

### 14. Hosting Of Third Party Information

The Platforms may from time to time host information provided by third parties that is intended for informational purposes only. FOOSH is in no way responsible to Users for the truthfulness, accuracy and legitimacy of the information so hosted and the User makes use of such hosted information entirely at their own risk. The User is responsible for evaluating the accuracy, completeness, and usefulness of any opinion, advice or other content available through the Platforms or obtained from a linked site.

### 15. Viruses

FOOSH make reasonable attempts to clear the Platforms of viruses and worms but cannot ensure that the Platforms will be at all times free from viruses, worms, malware, extortionware or other destructive software or software of a criminal nature. Users are urged to take appropriate safeguards before downloading any information from the Platforms. FOOSH accept no responsibility for any damage to User’s computer equipment, smart phones, tablets, data storage devices, data, images, videos, software or other property which may result from use of the Platforms or downloading anything from the Platforms.

### 16. Communications

The User expressly and irrevocably agrees to receive communications in any form whatsoever from FOOSH in relation to the Products and Services, the Platforms, offers, promotions and marketing materials from FOOSH. The User may opt out of receiving marketing material at any time by giving written notice to FOOSH by way of an email directed to admin@fooshapp.com. Your email should give details of the email address you wish to have removed from our database and should state clearly that you are the registered owner of the email address in question. You must also provide us with your street address and telephone number so that FOOSH can manually confirm your identity. Your details will be deleted from our marketing database as soon as the identity verification is completed. 

### 17. Disclosure Of Identity To Copyright Claimants

FOOSH at all times retains the right to disclose your identity to any third party who claims that you have violated their rights and or intellectual property or their right to privacy by posting the said information or other material on the Platforms provided always that any party making such a claim shall provide FOOSH with sufficient information and evidence supporting their claim and the sufficiency of such information and evidence shall at all times be at the sole and absolute discretion of FOOSH.

### 18. Term Of License And Termination

18.1 The term of the License granted by FOOSH to the User commences at the time the User starts to use the Platforms on each visit and the License shall continue until such time that the User exits the Platforms or FOOSH otherwise terminates the License at it sole and absolute discretion. 

18.2  FOOSH may at any time at its absolute discretion terminate or suspend the account of a User if in the opinion of FOOSH the User has breached any term of this TAC and/or whose conduct has brought disrepute to FOOSH or the Platforms or has in any way harmed the business and/or the goodwill of FOOSH and FOOSH shall be under no obligation whatsoever to provide the User with an explanation for such termination and/or suspension.

The termination and/or cancellation shall be effective on sending notice to the User at the last provided email address by the User. Upon such termination and/or suspension, FOOSH shall be at liberty to recover from the User damages in respect of any conduct and/or actions by the User leading to the termination and/or suspension. On termination of the account, the Registered User hereby expressly agrees not to register or attempt to register for another account on the Platforms.

On termination of the account of the User, any information, images, videos or other content of whatever nature shall no longer be accessible to the User and FOOSH shall not be liable to the User in any way for the inaccessibility and/or loss of all such information.

### 19. Disclosure Of The User’s Personal Information

19.1 FOOSH will not disclose the personal information of the User except pursuant to a valid court order and/or search warrant. In such event, unless restrained otherwise by the court order and/or search warrant, FOOSH shall give written notice to the User of such disclosure of information and the circumstance thereof and such notice will be sent by email to the User’s last provided email address. 

19.2 FOOSH shall at all times be at liberty to use your personal information in anonymous form for the purpose analysing the performance of the Platforms and such analysis may be carried out by a third party contractor. The third party shall only receive your data in anonymous form and shall be under a contractual obligation not to make any unauthorised use of your personal information or to disclose it to any other party

### 20. Mergers And Acquisitions

In the event that FOOSH and/or its assets, including but not limited to the Platforms and all Intellectual Property relating thereto, are purchased by a third party, in whole or in part, FOOSH shall at all times retain the right to transfer the personal details of Users and all information relating to and/or provided by Users to the Buyer subject to a confidentiality undertaking from the Buyer

### 21. Third Party Websites And Applications

In order to enhance its services, FOOSH may provide links to services provided by third parties on their respective platforms. It is expressly stated that FOOSH has no control or influence on the operation and/or content of these third party platforms. Accordingly, Users who elect to follow these links do so absolutely at their own risk they should read the TAC and Privacy Policy of such linked platforms.

In using the Platforms, the User may elect to connect his/her/its account with their Google or Facebook or Twitter accounts. You should be aware that the processes used in such connections may gather information about you, your use of the Platforms, your profile and your content and such processes are not within the control of FOOSH. If you make such connections to your account, you expressly agree to the following:

a) Connections to such social media are made entirely at your own risk and you will hold FOOSH and all its stakeholders harmless against any loss, economic or otherwise that you may suffer;

b) That you will indemnify FOOSH against any loss that it might suffer as a result of you making such connections; and

c) You consent to the sharing of any and all information that such applications may harvest on you.

### 22. Third Party Intellectual Property And Copyright Claims

FOOSH respects the intellectual property of all third parties and all Users of the Platforms are required to do the same. Users shall ensure that they do not infringe upon the intellectual property and copyright of any third party. If FOOSH finds the User is in breach this provision, it shall take the necessary steps to remove the offending items in accordance with the DMCA. FOOSH will also remove the offending items if it receives a DMCA Takedown Notice or is otherwise notified in writing together with the details of the claim to the intellectual property and/or copyright. FOOSH may at its absolute discretion terminate the account of an offending User.

### 23. Advertising

FOOSH may feature advertising on the Platforms and such advertising may feature links to third party websites. If you follow such links, you do so absolutely at your own risk and you shall hold FOOSH and all its stakeholders harmless against any loss, economic or otherwise that you may suffer as a result of following such links. The manner, placement and nature of the advertising shall be at the absolute discretion of FOOSH and it may change from time to time and FOOSH may not specifically identify paid and sponsored content and placements to you and you expressly consent to the presence of such advertising.

### 24. Request To Delete Information

The User may submit a request in writing at any time for private information to be removed from FOOSH’s database. FOOSH shall process such written notification from the Registered User within thirty (30) clear days from the receipt of such notice provided always that any Order IDs and any personal details attached thereto will not be deleted, removed and/or amended in any way. Your written request by way of email should be directed to admin@fooshapp.com. Your email should give details of the private information you wish to have removed and your full details and contact telephone number for identity verification purposes.

### 25. Personal Injury Liability

FOOSH shall not in any way be liable to any User who suffers any personal injury of whatever nature in the process of taking up any Merchant offers on the Platforms. Users shall take such Merchant offers on their own volition and do so entirely at their own risk and FOOSH gives no direction, inducement, enticement or encouragement whatsoever, direct or indirect, to Users to take up such offers.

### 26. Independent Parties

For avoidance of doubt, it is expressly stated that FOOSH shall at all times be an independent service provider to the User and nothing shall indicate, expressly or impliedly, that FOOSH is an agent, partner and/or employee of the User or vice versa or that FOOSH is in any way connected to the User.

### 27. Cookies

FOOSH collects information about visitors to the Platforms through the use of Cookies. For information on FOOSH’s policies relating to the use of cookies, please view FOOSH’s Cookies Policy.

### 28. Privacy Policy

In the course of your use of the Platforms , FOOSH collects information on Users. Some of this information is personally identifiable information relating to your identity, for example the information you provide us when you register an account on the Platforms . This information is captured, processed and stored in accordance with FOOSH’s Privacy Policy . Such Privacy Policy forms an integral part of this TAC and the two documents should be read in conjunction with each other and also with our Cookie Policy. By using the Platforms, you expressly agree to FOOSH collecting and processing personal information on you.

### 29.   Disputes And Arbitration

29.1 In the event of any dispute between FOOSH and the User, both parties hereby expressly and irrevocably agree that such dispute shall be settled exclusively by way of arbitration and that they shall jointly agree to appoint an arbiter located in England for this purpose. In the event that FOOSH and the User are unable to decide on a mutually acceptable arbiter, the selection of the arbiter shall be made by the President/Chairman for the time being of the Chartered Institute of Arbitrators in London. The basis of the arbitration shall be based on the laws and regulations relating to arbitration in England and Wales and it is expressly agreed that the arbitration laws and regulations in any and all other jurisdictions is/are expressly excluded. The decision of the arbitrator shall be final and binding on FOOSH and the User provided always that the arbitrator shall not make any decision or award that would change, cancel, rescind any provision of this TAC and any decision or award shall be consistent with provisions and intent of this TAC. In any event, the Registered User shall not be at liberty to raise any dispute in respect of any order until the lapse of ninety (90) clear days from the date the dispute arises. All information relating to any disputes between FOOSH and the Registered User, whether in arbitration or not, shall be confidential and the parties shall not disclose such information to any third party, including on social media, on any basis except as specifically required by the arbitration proceedings.

29.2 The User agrees that any claims brought by him/her/it against FOOSH shall be brought in his/her/its own name and capacity and not as a member of any class or purported class or representative proceedings.

29.3 FOOSH and the User agree that the arbitrator shall not be entitled to consolidate claims of more than one person and the arbitrator may not preside over any class action or representative proceeding and must preside on any and every claim against FOOSH on an individual basis and on the merits of each claim individually.

29.4 The arbitrator shall only be entitled to may awards on an individual claim basis and shall not make awards in respect of a more than one claim.

29.5 Where it deems necessary to avoid litigation, FOOSH shall have the right at all times to adopt the costs of arbitration proceedings where the User objects that the cost of the arbitration proceeding will be excessive compared to litigation costs and the User provides valid evidence of such cost comparison.

29.6 All arbitration proceedings shall be private and confidential to the fullest extent permissible by law in any relevant jurisdiction.

29.7 Except as otherwise provided and/or as provided by the arbitrator, parties to an arbitration proceeding shall pay for their own legal and filing fees.

### 30.   Modifications And Amendments To The TAC

FOOSH reserves the right to modify and amend the TAC as it see fit and at its absolute discretion and such amendments and modification shall become effective immediately on the new TAC being posted on the Platforms . Any use by Users of the Platforms after any modification or amendment of the TAC is effective shall be taken as an express agreement by Users to the amended TAC. It shall be the sole responsibility of Users to review the TAC posted on the Platforms from time to time to review any modifications and amendments and the User expressly acknowledges and accepts such responsibility. If Users do not agree to the modifications and amendments, they should immediately stop and refrain use of the Platforms and should immediately cancel their account.

### 31.   Successors And Assigns

This TAC and the agreement within it in respect of the terms and conditions set out herein shall be binding on and shall enure to the benefit of both parties and their respective successors and, heirs, executors and permitted assigns.

### 32. General Provisions

a)  **Delays, Indulgences And Omissions** - A delay or indulgence or omission in exercising any right, power or remedy pursuant to the terms of this Agreement shall not in any way impair or be construed as a waiver of such rights, powers or remedies.

b)  **Exercise Of Rights** - Any exercise of any rights, powers and remedies or non-exercise thereof shall not preclude or restrict any other or further exercise of such rights, powers or remedies.

c)  **Waivers** - A waiver of any breach of any term(s) or of a default shall not constitute or be deemed to be a waiver of any other breach or default that shall already have occurred at the date of such waiver or which may occur in the future and the waiver of any breach or default shall not preclude the party subsequently requiring compliance.

d)  **Cumulative Remedies** - All rights and remedies are cumulative and not exclusive of any rights, powers and remedies granted by law.

e)  **Severability** - If any provision or term is or shall become illegal, invalid or unenforceable under the laws of any jurisdiction such event shall not in any way affect or impair the validity and enforceability of any other provisions and terms.

f)  **Notices -** The User expressly consents to receiving any and all notices from FOOSH by electronic means and FOOSH shall give notices to the User by means of an email addressed to the User at the last email address provided by the User and/or posting a notice on the Platforms. Notices of a legal nature may be sent to FOOSH at admin@fooshapp.com. 

g) **Entire Agreement -** This TAC represents and constitutes the entire understanding and relationship between FOOSH and the User and it supersedes any and all prior understandings and undertakings between them.

h) **Third Party Rights** – The benefits, terms and conditions or any other aspect of this TAC may not be enforced by any person or entity who is not a party to this TAC and the provisions of the Contracts (Rights of Third Parties) Act 1999 are expressly excluded except in respect of the Apple App Store insofar as its interest extends to the Foosh App.

i) **Interpretation -** Headings in this TAC are for convenience only and shall not be used in the interpretation of any provision or terms of this TAC.

### 33. Data Protection Act 2018 And GDPR Compliance

FOOSH respects and complies with theData Protection Act 2018 and EU General Data Protection Regulations (GDPR). Some of the keyways FOOSH will comply with these regulations are:

a) Data Controller

FOOSH may if necessary, process your personal data for certain internal purposes as a Data Controller. For example, to comply with its legal obligations, billing purposes and other administrative support functions. FOOSH may also process the information for the purposes of marketing its products and services to existing customers. Except for services like Google Analytics and Mailchimp, FOOSH does not use the services of third party contractors to process your data and at this time FOOSH does not intend to change this policy. If this changes in the future, all third party contractors will be put under a contractual obligation to maintain the security of your personal data and not to use it for any purposes not authorised by the User and FOOSH.

b) Consent

FOOSH asks that the User actively consent to any contact from it and FOOSH will explain clearly what the User are consenting to. The User have the right to withdraw any consent given to FOOSH at any time and the User can do so without fear of any repercussions of any kind. FOOSH will however cease to provide the service(s) for which the User withdraws consent. The User may withdraw consent by sending us an email stating what consent is withdrawn. The withdrawal of consent will be implemented within thirty (30) days of days of the written notice.

c) Breach Notification

In the event FOOSH is in breach of any term or condition, it will notify the User immediately it becomes aware of the breach. This, in particular, applies to any breach to security in relation to your personal information. 

d) Right to Access

The User has the right to request information as to whether personal data relating to them is being stored and processed, where it is being processed and for what purpose. The User is entitled to request for a copy of the personal data retained by FOOSH and it shall provide the User with a copy of the said information by way of email.

e) Right to be Forgotten

The User has a right to request for personal data retained by FOOSH to be deleted. Where possible, FOOSH will endeavour comply with the request within thirty (30) days of receiving it. However, in certain instances FOOSH will not be able to comply with the request. For example, where the law requires FOOSH to retain the data or where FOOSH has to retain some of your personal data for our accounting and taxation purposes; 

f) Data Portability

The User has a right to deal with his/her/its personal data as he/she/it see fit. At the User’s request, FOOSH will provide a copy of the personal data.

g) Privacy

FOOSH has implemented appropriate technical and organizational measures to comply with GDPR regulations and to protect the User’s personal data. FOOSH will hold and process only the absolutely necessary data to allow for the efficient running of its business and the Platforms and it limits access to the User’s personal data to those who need to.

Where FOOSH processes any of the User’s personal data for any reason whatsoever, FOOSH shall:

i) require the User’s written request where the processing to be performed is at his/her/its request;

ii) ensure that persons authorised to process the personal data are committed to confidentiality or are under an appropriate statutory obligation of confidentiality;

iii)take all measures as are required pursuant to Article 32 of GDPR for Security of Processing;

iv)respect the conditions referred to in paragraphs 2 and 4 of Article 28 of GDPR for engaging another Data Processor (referred to as a Sub- Processor);

v) make available to the User information necessary to demonstrate compliance with GDPR regulations.

### 34. CCPA Compliance

Since 1st January, 2020, the California Consumer Privacy Act (“CCPA”) afforded specific rights to consumers to ensure that their privacy and their personal data is protected. While FOOSH does not fall within the ambit of the CCPA, it will voluntarily comply with its requirements on a purely voluntary basis for the benefit of our US Users.

The rights conveyed by CCPA include the right to require businesses to disclose information about the collection and use of their personal information over the past 12 months. You may also ask us to delete any personal information that FOOSH will collect from you. In the event that FOOSH decide to sell the personal information of Users, something that will never happen, the User have the right to opt out of such sale. In the event that the User exercises any of his/her/its rights conferred by CCPA, FOOSH is are not allowed to discriminate against the User for exercising his/her/its rights.

For the purposes of CCPA, FOOSH would be classified as a ‘Service Provider’. FOOSH operates a loyalty and rewards program that allow merchants to reward their loyal customers with discounts and special offers. In doing so, FOOSH will collect information on the User to facilitate the efficient functioning of the Foosh App. As you make purchases from signed up Merchants and applying the discounts and offers, FOOSH will make your usage history data available to the Merchants through the Foosh App .

Should the User wish to delete any of his/her/its information collected by FOOSH, the User should send a written request to admin@fooshapp.com. FOOSH will assess the request and comply with it if the request does not wall within our exceptions to meet the User CCPA requests. These exceptions mainly relate to legal and statutory regulations which require FOOSH to maintain certain User information. These usually relate to accounting and tax matters

### 35. Governing Law

This TAC shall be governed solely by the laws of England and Wales. The laws and regulations of any other jurisdictions are expressly excluded notwithstanding that the Users may be resident in jurisdictions other than England and Wales. The United Nations Convention on Contracts for the International Sale of Goods is expressly excluded and the courts of England and Wales shall have jurisdiction.

### 36. Use Outside Jurisdiction

36.1 The Platforms and the and all services rendered through them are for all purposes domiciled in England and Wales. FOOSH does not grant access to the Platforms to any Users who are located in any jurisdictions where it would not be legal, whether by national or international law, for the Use of the Platforms.

36.2 If you are located outside of England and Wales, the Platforms may not be suitable for you and FOOSH makes no representations in this respect. The onus shall be on Users to determine whether they are entitled to access the Platforms from their respective jurisdictions.

36.3 In using the Platforms, the User represent to FOOSH that they made the necessary checks to determine that they are entitled to use the Platforms in their respective jurisdictions.

36.4 Any taxes of whatever nature that may arise as a result of using the Platforms from outside the Jurisdiction shall be solely and exclusively for the account of the User.

### 37. Contact

You may contact FOOSH for any purposes at admin@fooshapp.com. 

### 38. Effective Date

This TAC is effective from the 28th of February, 2021 and shall remain in effect until such time it is superseded by an update or terminated by FOOSH.


### TERMS AND CONDITIONS

**Provisions Specific To The**

**Foosh Mobile Application (Foosh App)**

All definitions stated in the main body of the TAC apply equally in this section.

The following terms and conditions are applicable only to Foosh App in compliance with the requirements of the Apple App Store and/or Google Play.

1. Foosh App may be downloaded either from the Apple App Store or Google Play as appropriate for your device. Both the iOS and Android versions are free of charge to the User to download and install. Your use of either version is conditional on your acceptance of the Terms and Conditions set out in the TAC, including but not limited to this Section of the TAC. If you do not agree with the Terms and Conditions or you do not wish to be bound by them, please uninstall Foosh App and delete all associated files.

2. On any Apple device, the User agrees to use only the iOS operating system and on any Android device, the User agrees to use only the Android operating system to access and use the Foosh App and the User expressly agrees to and accepts the entirety of the Terms of Service of the Apple App Store ([https://www.apple.com/legal/internet-services/itunes/us/terms.html](https://www.google.com/url?q=https://www.apple.com/legal/internet-services/itunes/us/terms.html&sa=D&source=editors&ust=1614101099631000&usg=AOvVaw0pFcsUT9NjB2-vlAPO0KdG)) and Google Play ([https://play.google.com/intl/en-us_us/about/play-terms/index.html](https://www.google.com/url?q=https://play.google.com/intl/en-us_us/about/play-terms/index.html&sa=D&source=editors&ust=1614101099632000&usg=AOvVaw3DI80OSwNl2kUJnGx_MD_g)) as appropriate.

3. The User agrees that they will only use applications sourced from the Apple App Store on any Apple device owned by the User or that the User is in control of as specified in the Apple App Store Terms of Service. The User agrees that they will only use applications sourced from Google Play on any device owned by the User or that the User is in control of that uses the Android operating system as specified in Google Play Terms of Service.

4. The User agrees that FOOSH is the party solely responsible for the Foosh App and that the Apple App Store and Google Play accept no responsibility for any loss, direct or indirect, incurred by the User as a result of using the Foosh App.

5. The User acknowledges that the Apple App Store and/or Google Play are under no obligation to provide any services and/or updates and/or maintenance of whatever nature in relation to the Foosh App. All maintenance, service and other issues relating to the Foosh App should be directed to FOOSH.

6. Apple App Store and Google Play at all times retain the right to suspend the availability of the Foosh App at any time without notice or explanation.

7. The User agrees that the Apple App Store and/or Google Play shall have no liability in respect of the Foosh App beyond the price that may have been paid by the User to the Apple App Store or to Google Play to gain usage of Foosh App and any other claims that the User may have in relation to Foosh App will solely be the responsibility of FOOSH and which responsibility is conditional on the provision of this TAC.

8. The User agrees that the Apple App Store and/or Google Play is not the party responsible for dealing with any claims in respect of Foosh App relating to product liability, its lack of performance and/or its failure to meet any legislative or regulatory requirements and the User expressly agrees to direct all such claims directly to FOOSH and not tot the Apple App Store or to Apple and/or to Google Play, Google or Alphabet.

9. The User agrees that the Apple App Store and/or Google Play is/are not responsible for any breach of any third party intellectual property rights and that such responsibility rests solely with FOOSH.

10. By downloading Foosh App from the Apple App Store or Google Play, the User warrants to the Apple App Store and Google Play respectively that:

    a. they are not located in a country which is the subject of US Government embargo or sanctions of any type or which has been referenced as a terrorist supporting country.

    b. or that the User belong to any organisation that has been designated a terrorist organisation and that the User is not listed on any restricted party list.

11. FOOSH and the User agree that Apple and the Apple App Store and Google Play, Google and Alphabet are third party beneficiaries of this TAC and that the agreement between FOOSH and the User in relation to this TAC shall entitle Apple and/or Google to enforce this TAC against the User as a third party beneficiary as relates to any applications downloaded from the Apple App Store and Google Play and FOOSH and the User agree to the same.

12. The Foosh App is made available to the User on license on a “AS IS” basis without charge subject to the terms and conditions of this entire TAC. To the maximum extent permitted by the law in the Jurisdiction and/or any other relevant jurisdiction neither Apple App Store or Google Play accept any liability in respect of the Foosh App and neither do they , jointly or severally, extend any warranty or undertaking or accept any condition or other terms in respect of the use of Foosh App by the User and the User expressly without reservation states that he/she/it uses the Foosh App entirely and exclusively at his/her/own risk and further agrees that any loss of any kind incurred/suffered by the User shall be borne entirely by the User and the User agrees to make no claims on Apple App Store and/or Google Play.

13. Neither Apple App Store nor Google Play shall be required to provide Users with any refund in respect of any payments made by the User to FOOSH or the Merchants.
