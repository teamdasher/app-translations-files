## Privacy

Foosh Limited (“We”, “Foosh”, “our”, or “us”) are committed to protecting your (“you” or “your”) personal data, handling it responsibly and respecting your privacy.

This policy (together with our Terms Conditions available in the Foosh app and at https://www.fooshapp.com/terms-and-conditions) covers the personal data that we collect whenever you interact with us, including when you visit our website or use the applications (together the “Apps”) featured on it and when you correspond with us (such as by email or over the phone). It also covers personal data that we may receive from third parties.

The sections below explain in more detail:
- the types of personal data we collect from you
- the types of personal data we receive from third parties
- why we process your personal data
- who we share your personal data with
- personal data transfers outside of the EEA
- how long we retain your personal data and security
- your rights to withdraw your consent and to object (including to direct marketing)
- your other personal data rights
- how to contact us and exercise your rights


### Personal data Foosh collects from you

We collect personal data from your interactions with us, such as when you engage with our websites and apps or when you email or phone us.

The personal data we collect from you includes:
- name and contact details
- merchants that you request, select or search, book and visit
- information that you create including photos, reviews and bookings, vouchers redeemed and loyalty that you earn, share and claim
- your location when you interact with the Foosh app or website
- content you have created on our forums or social media
- Information about support problems you have reported to us


### Personal data Foosh receives from third parties

Sometimes we receive personal data from third parties, in particular:
- software tell us how you engage with our Apps
- government and law enforcement authorities may provide us with personal identification and background information when they are involved in official inquiries

### Why Foosh processes your personal data

This section explains the reasons why we process your personal data and our legal bases for doing so:

- information about your visits and interactions with merchant business on the Foosh platform and which other Foosh users you choose to share contents and rewards are required to record, store and calculate your entitlement to loyalty rewards and offers provided by those businesses
- your location is use to verify your proximity to merchant businesses on the Foosh platform to validate the authenticity of requests and communications with merchants as well as to provide location relevant offers and communications 

#### Consent

If you’ve opted-in to receive information and offers relating to Foosh and our commercial partners (for example by ticking a box on one of our websites or apps), then we’ll provide this information to you by email, post, app notification, or phone.

We also rely on your consent to process information about your use of our websites, apps and social media sites, so that we can improve your browsing experience and deliver online advertising that is relevant to you.

Wherever we rely on your consent to process personal data, you have a right to withdraw that consent. [Click here for information on how to contact us and exercise your rights.](http://www.fooshapp.com/gdpr)

### Legitimate interests

We process your personal data when necessary to pursue our legitimate interests in the following:

- to notify you about changes to our services;
- to provide you with information about other merchants, goods and services we offer that are similar and to provide you with the information, products, and services that you request from us or which are complementary to those that you have already purchased or enquired about;
- to carry out our obligations arising from any contracts entered into between you and us;
- to provide you, or permit selected third parties to provide you, with information about goods or services we feel may interest you. If you are an existing customer, we will only contact you by electronic means (e-mail, notification or chat) with information about 3rd party merchant goods and services we think are of interest to you. If you are a new customer, and where we permit selected third parties to use your data, we (or they) will contact you by electronic means only if you have consented to this. Once you have given your consent, if you no longer want us to use your data in this way you will need to contact us at support@fooshapp.com so that we can remove you from this list or alternatively you can follow the unsubscribe option contained in each marketing communication;
- to ensure that content from our Apps is presented in the most effective manner for you and for your computer or device.
- to make suggestions and recommendations to you and other users of our Apps about goods or services that may interest you or them, and as part of our efforts to keep our Apps safe and secure;
- to administer our Apps and for internal operations, including troubleshooting, data;
- to improve our Apps to ensure that content is presented in the most effective manner to allow you to participate in interactive features of our services when you choose to do so; and
- to measure or understand the effectiveness of advertising we serve to you and others and to deliver relevant advertising to you.
- to verify you are a legitimate user of the platform and using the platform in accordance with its purpose, rulesw and terms and conditions.

You have a right to object to any processing that we undertake for our legitimate interests. [Click here for information on how to contact us and exercise your rights](http://www.fooshapp.com/cookiepolicy).

### Contract

We process your personal data when necessary for contractual reasons, such as to administer your account registration and to provide products and services that you have requested.

### Legal obligation

We are legally required to process your personal data in cases where we need to respond to certain requests by government or law enforcement authorities.

### Who Foosh shares personal data with

We will share your personal data with the following recipients:
- 3rd party merchants, so they can advise us how best to communicate with you on their behalf.  We do not give your data to partners for them to communicate with you directly.
- third party suppliers involved in data insights; website hosting; advertising; systems maintenance; database management; identity checking; payment processing; delivery logistics; and
- government authorities to assist with their official requests and comply with our legal obligations.

### Personal data transfers outside of the EEA

The EEA includes all EU countries, as well as Iceland, Liechtenstein and Norway.  Some of the third parties that we share personal data with, may need to transfer personal data outside of the EEA, for example to the United States of America.

Where your personal data is transferred to a country outside of the EEA and that country is not subject to an EU adequacy decision, we will ensure your data is protected by appropriate safeguards (for example, EU approved standard contractual clauses, a Privacy Shield certification, or a supplier’s Binding Corporate Rules).  A copy of the relevant safeguard can be provided for your review on request – [click here for information on how to contact us and exercise your rights](http://www.fooshapp.com/gdpr).

### How long Foosh retains personal data and security

We retain personal data about your membership for as long as your account remains active, and for 5 years after (in case you decide to reactivate your membership or have queries about it).

We retain personal data relating to your subscriptions for 5 years from the date of the relevant transaction.  This is to understand your preferences and to meet our legal and contractual obligations.

Where you have asked us not to send you direct marketing, we keep a record of that fact to ensure we respect your request in future.

We also retain information with the potential to give rise to legal disputes for 7 years from the expiry or termination of any contracts.

We seek to use reasonable organizational, technical and administrative measures to protect personal information within our organization.

Unfortunately, no data transmission or storage system can be guaranteed to be 100% secure.  If you have reason to believe that your interaction with us is no longer secure (for example, if you feel that the security of any account you might have with us has been compromised), please immediately notify us of the problem by contacting us. [Click here for information on how to contact us and exercise your rights](http://www.fooshapp.com/gdpr).

Unfortunately, the transmission of information via the internet is not completely secure. Although we will do our best to protect your personal data, we cannot guarantee the security of your data transmitted to our site; any transmission is at your own risk. Once we have received your information, we will use strict procedures and security features to try to prevent unauthorised access.
Your rights to withdraw consent and to object (including to direct marketing)

Wherever we rely on your consent to process personal data, you always have a right to withdraw that consent.

You also have the right to object to any use of your personal data for direct marketing purposes, as well as to processing that we undertake based on our legitimate interests.

[Click here for information on how to contact us and exercise your rights.](http://www.fooshapp.com/gdpr)

### Your other personal data rights

In addition to your rights to withdraw your consent and to object, you have the right to ask us:
- for access to information about your personal data or for a copy of your personal data
- to correct or delete your personal data
- to restrict (i.e. stop any active) processing of your personal data
- to provide you with certain personal data in a structured, machine-readable format and to transmit that data to another organisation

These rights may not always apply, for example, if fulfilling your request would reveal personal data about another person, or if you ask us to delete information which we are required by law to keep or have a compelling legitimate interest in keeping.  If this is the case then we’ll let you know when we respond to your request.

### How to contact us and exercise your rights

The easiest way to stop receiving information from us is by notifying us at [support@fooshapp.com](mailto:support@fooshapp.com).

We will send you an email confirmation to let you know that your data has been deleted from our Marketing records. Also if you object to any data processing provided by us, you can notify us at [gdpr@fooshapp.com](mailto:gdpr@fooshapp.com).

We will do our best to assist with any queries you have about your personal data.  You can contact us at any time using the contact details below.  When you do so, please provide your full name, your preferred contact information, and a summary of your query.

```
Foosh Limited
The Old Casino
28 Fourth Avenue
Hove
East Sussex
BN3 2PJ
```

If you consider our use of your personal information to be unlawful, you have the right to complain to an EU data protection authority where you live, work or where you believe a breach may have occurred. This is likely to be the Information Commissioner’s Office in the UK (please see further information on their website: [www.ico.org.uk](https://wwww.ico.org.uk)).

The information set out in this policy is provided to individuals whose personal data we process as data controller, in compliance with our obligations under Articles 13 and 14 of the General Data Protection Regulation 2016/679.

**This privacy policy was last updated on 06 April 2021.**

We suggest that you check back here for updates, as we will update this policy from time to time. By continuing to use the Apps after such changes have been made, you acknowledge and agree you have read, understood and accept the changes.
