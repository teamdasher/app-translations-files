## App translation files repo

Repo that contains translations for the native app as well as translations of legal documents

Before doing anything please install all the dependencies!
After cloning the repo run:

```
yarn
```

### App translations
Modify the appropriate json. On commit there's a script checking JSON validity.

### Legal documents
Legal documents are places in the `md` folder. To convert them to html, run `yarn md2html`. That will create files in the html folder.
