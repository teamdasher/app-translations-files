const path = require("path");
const fs = require("fs");
const marked = require("marked");

const INPUT_DIR = "./md/";
const OUTPUT_DIR = "./html/";

function htmlTemplate(inner) {
  return `<html lang="en">
            <head>
              <meta charset="UTF-8">
              <meta name="viewport" content="width=device-width, initial-scale=1">
            </head>
            <body>${inner}</body>
          </html>`;
}

function findInDir(dir, filter, fileList = []) {
  const files = fs.readdirSync(dir);

  files.forEach((file) => {
    const filePath = path.join(dir, file);
    const fileStat = fs.lstatSync(filePath);

    if (fileStat.isDirectory()) {
      findInDir(filePath, filter, fileList);
    } else if (filter.test(filePath)) {
      fileList.push(filePath);
    }
  });

  return fileList;
}

const hasPathPrefix = (relativePath) =>
  relativePath.substr(0, 2) === ".." ||
  relativePath.substr(0, 2) === "./" ||
  relativePath.substr(0, 2) === ".\\";

const formatPath = (dir, separator = "/") => {
  const oppossiteSep = separator === "/" ? "\\" : "/";
  return dir.replace(new RegExp(`\\${oppossiteSep}`, "g"), separator);
};

const getRelativePath = (file, fromDir) => {
  let relativePath = path.relative(
    formatPath(fromDir, path.sep),
    formatPath(file, path.sep)
  );
  if (!hasPathPrefix(relativePath)) relativePath = `./${relativePath}`;
  return formatPath(relativePath);
};

const ensureFileDirectoryExists = (filePath) => {
  const directory = path.dirname(filePath);

  if (!fs.existsSync(directory)) {
    fs.mkdirSync(directory);
  }
};

function convert(name) {
  return function (err, data) {
    if (err) {
      throw err;
    }
    const html = marked(data);
    const fullHtml = htmlTemplate(html);
    ensureFileDirectoryExists(OUTPUT_DIR);
    const outputPath = formatPath(`${OUTPUT_DIR}${name}.html`);
    fs.writeFileSync(outputPath, fullHtml);
  };
}

function findMD() {
  const mds = findInDir(INPUT_DIR, /\.md$/);
  console.log(`Markdown to HTML converter script`);
  console.log(`Found ${mds.length} files in ${INPUT_DIR} dir`);
  mds.map((md) => {
    const p = getRelativePath(md, path.dirname("."));
    const fileName = path.basename(p, ".md");
    const data = fs.readFile(p, "utf8", convert(fileName));
  });
  console.log(`Created output HTMLs in ${OUTPUT_DIR} dir`);
}

findMD();
